- Create tuple.
- unpack tuple
- pass tuple as argument to unpack *tuple.
- unpack tuple from a function return tuple.
- run a for and unpack correctly: (("Sao Paulo", "SP", (-23.5691939,-46.6576797),("Rio de Janeiro" , "RJ", (22.9138851,-43.7261746))))

## Grabing the rest:
__a, b, *rest = range(5)__

returns 0,1 [2, 3, 4]

__a, *body, c, d = range(5)__

returns 0, [1, 2], 3, 4


