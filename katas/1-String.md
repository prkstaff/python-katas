# The String Kata

## Objective
Be familiar with vast string formats in python 
-  Number formating
-  float formating
-  American Date formating
-  Brazilians
-  using %
-  using {} and str.format
-  using format dict like
-  formating with '' and without eg: a

- '{} {}'.format('one', 'two')
- using % is old
- '{1} {0}'.format('one', 'two')
- '{0!s} {0!r}'.format(Data()
- https://pyformat.info
- '{:.5}'.format('xylophone')
- '{:10.5}'.format('xylophone')
- '{first} {last}'.format(**data)
- '{first} {last}'.format(first='Hodor', last='Hodor!')
- '{d[4]} {d[5]}'.format(d=data)
- '{:%Y-%m-%d %H:%M}'.format(datetime(2001, 2, 3, 4, 5))
- '{:.{prec}} = {:.{prec}f}'.format('Gibberish', 2.7182, prec=3)
- the __format__ method


## Steps
- Create a Person class with __str__ representation
- Also, one of the person is the 007, you have to print her agent number as 007
- Printing the above should return A readable descripting of the informations
- Print the birth date and time in Brazillian format and American Format.
  using pytz

