# French Deck Kata

## Proposal:
- Write a class for a French Deck (classic deck with 52 cards)
- get a random card
- get the length of the deck
- get the last 3 cards
- get the first 3 cards
- Loop throught the cards and print the Hearts
- Check if Card("Q", "Heart") in FrenchDeck
- print the biggest card and all the deck using sorted

## Understandings:

Praticing this kata will allow you to understande the powers os
__getitem__, __len__ and namedtuples