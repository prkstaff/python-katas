import datetime
import pytz

class Person:
    def __init__(self, name, age, gender, profession, salary, location, 
    agent_number=None):
        self.name = name
        self.age = age
        self.gender = gender
        self.profession = profession
        self.salary = salary
        self.location = location
        self.agent_number = agent_number
        self.register = datetime.datetime.now(tz=pytz.timezone("America/Sao_Paulo"))

    def salut(self):
        salut_message = "Hi! I am {0.name}, a {0.gender}, I am {0.age} years old, "
        "a {0.profession} , living in {0.location}, I earn {0.salary} a month."
        if self.agent_number:
            salut_message += salut_message + "\n Also I am the {0.agent_number:03d} agent"

        print(salut_message.format(self))

    def __str__(self):
        return "{0.name}, registered at {0.register:%Y-%m-%d %H:%M} ".format(self)


susan = Person(name="Susan", age=45, gender="Female", profession="Finances Manager",
               salary=5.500, location="Sao Paulo", agent_number=7)

susan.salut()
print(susan)

