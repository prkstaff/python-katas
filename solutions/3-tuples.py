import collections

Cities = collections.namedtuple("State", ["name", "state", "cords"])

latin_cities = [
    Cities(name="Rio de Janeiro", state="RJ", cords=(22.9138851, -43.7261746)),
    Cities(name="São Paulo", state="SP", cords=(-23.5691939, -46.6576797))]

for name, state, (lat, lang) in latin_cities:
    print(name, state, lat, lang, "\n", sep="\n")
