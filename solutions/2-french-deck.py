import collections
from random import choice

Card = collections.namedtuple("Card", ['rank', 'suit'])


class FrenchDeck:
    def __init__(self):
        self.ranks = [x for x in range(2, 11)] + list("JQKA")
        self.suits = "Diamond  Spade Heart Club".split()
        self.cards = [Card(suit=suit, rank=rank) for suit in self.suits for rank in self.ranks]

    def __getitem__(self, item):
        return self.cards[item]

    def __len__(self):
        return len(self.cards)

    def rank_card(self, card):
        return self.ranks.index(card.rank) * 4 + self.suits.index(card.suit)


deck = FrenchDeck()

random_card = choice(deck)

deck_length = len(deck)

three_lasts = deck[3:]

first_three = deck[:3]

if Card(rank="Q", suit="Heart") in deck:
    print("Q of Hearts is in French Deck")

for card in [x for x in deck if x.suit == "Heart"]:
    print(card)

print("max card", max(deck, key=deck.rank_card))

print(sorted(deck, key=deck.rank_card))
